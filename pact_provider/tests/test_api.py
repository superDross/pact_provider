import os

import pytest
from pact import Verifier

PACT_UPLOAD_URL = (
    "http://127.0.0.1:9292/pacts/provider/FullName/consumer/FullNameClient/latest"
)

PACT_MOCK_HOST = "localhost"
PACT_MOCK_PORT = 1234
PACT_MOCK_URL = f"http://{PACT_MOCK_HOST}:{PACT_MOCK_PORT}"

PACT_BROKER_URL = "http://pact-broker:9292"
PACT_BROKER_USERNAME = "broker"
PACT_BROKER_PASSWORD = "broker"

# define where the pact files will live: /consumer/tests/pacts/
THIS_DIR = os.path.dirname(os.path.realpath(__file__))
PACT_DIR = os.path.join(THIS_DIR, "pacts")
LOG_DIR = os.path.join(THIS_DIR, "logs")


def test_name(test_client):
    """
    Traditional test
    """
    resp = test_client.get("/create?forename=david")
    assert resp.json.get("fullname") == "David McDanger"


# @pytest.fixture
# def default_opts():
#     return {
#         "broker_username": PACT_BROKER_USERNAME,
#         "broker_password": PACT_BROKER_PASSWORD,
#         "broker_url": PACT_BROKER_URL,
#         "publish_version": "3",
#         "publish_verification_results": False,
#     }


# def test_get_user_non_admin(default_opts):
#     verifier = Verifier(provider="FullName", provider_base_url=PACT_MOCK_URL)

#     output, logs = verifier.verify_with_broker(
#         **default_opts,
#         verbose=True,
#         provider_states_setup_url=f"{PACT_MOCK_URL}/create"
#     )

#     assert output == 0
