import pytest

from app import app


@pytest.fixture(scope="module")
def test_client():
    # TODO: understand what is going on here
    # resource: https://www.patricksoftwareblog.com/testing-a-flask-application-using-pytest/
    # checkout the official docs for another fixture: https://flask.palletsprojects.com/en/1.1.x/testing/

    # Flask provides a way to test your application by exposing the Werkzeug test Client
    # and handling the context locals for you.
    testing_client = app.test_client()

    # Establish an application context before running the tests.
    ctx = app.app_context()
    ctx.push()
    yield testing_client
