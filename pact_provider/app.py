from typing import Dict
from flask import Flask, request, jsonify


app = Flask(__name__)


@app.route("/create", methods=["GET"])
def get_surname() -> Dict[str, str]:
    """
    Creates surname based on given forename.
    """
    forename = request.args.get("forename")
    if not forename and len(forename) < 1:
        raise ValueError("forename arg must be parsed")
    surnames = {"x": "Guy", "j": "Shufflebottom"}
    surname = surnames.get(forename[0].lower(), "McDanger")
    return jsonify({"fullname": f"{forename.title()} {surname}"})


@app.route('/_pact/provider_states', methods=['POST'])
def provider_states():
    """
    This endpoint is used to setup the test data that needs to be present
    for the pact file(s) to be verified.

    Needs to be parsed to the `pact-verifier --provider-states-setup-url` argument
    when verifying the pact files.

    This is not an absolute requirement. You only have this handler if you need
    to setup dummy data on the providers database.

    This does nothing as we have no datastore.
    """
    return {}


if __name__ == "__main__":
    app.run(host="127.0.0.1", port=8067, debug=True)
    # requests.get("http://127.0.0.1:8067/create", params={"forename": "david"}).json()
