FROM ubuntu:20.04

USER root

# hadolint ignore=DL3008
RUN apt-get update && apt-get install -y --no-install-recommends \
      python3.8 \
      python3-pip \
      && apt-get clean \
      && rm -rf /var/lib/apt/lists/*

ARG PORT=8067
ARG CWD=/usr/src/apps/pact_provider/
ARG ENV=${ENV:-prod}

WORKDIR ${CWD}
COPY . ${CWD}

RUN groupadd --gid 999 appuser && \
    useradd \
      --system \
      --create-home \
      --uid 999 \
      --gid appuser \
      appuser && \
    chown  -R appuser /usr/src/apps/pact_provider/

USER appuser

RUN pip3 install --user -r requirements.txt

EXPOSE ${PORT}
ENV PYTHONPATH=${CWD}
ENV FLASK_APP=pact_provider/app.py
ENV PORT=8067

CMD python3 -m flask run --host 0.0.0.0 --port ${PORT}
