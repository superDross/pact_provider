build:   ## build container
	docker-compose -f docker-compose.yml build ${BUILD_ARGS}

build-no-cache:  ## build docker development image from scratch
	BUILD_ARGS=--no-cache $(MAKE) build

up:        ## spin up the pact provider container
	docker-compose -f docker-compose.yml up -d

down:        ## spin down the pact provider container
	docker-compose -f docker-compose.yml down

logs:
	docker-compose -f docker-compose.yml logs -f

test:    ## test consumer code and upload pact to broker
	docker-compose -f docker-compose.yml run pact_provider python3 -m pytest

simulate-ci:
	docker-compose -f docker-compose.yml run pact_provider python3 -m pytest

test-ci:
	# does not work as there is no git repo in the container
	docker-compose -f docker-compose.yml run pact_consumer gitlab-runner exec shell test

help:              ## show this message
	@echo
	@echo Targets:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/##//' -e 's/^/  /' -e 's/://'
	@echo
